## [1.0.2](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/compare/v1.0.1...v1.0.2) (2020-02-03)


### Bug Fixes

* updated to ghidra 9.1.1 ([4f9e817](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/commit/4f9e817))

## [1.0.1](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/compare/v1.0.0...v1.0.1) (2020-01-24)


### Bug Fixes

* docker push updates ([4509546](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/commit/4509546))

# 1.0.0 (2020-01-24)


### Bug Fixes

* remove extra line ([02619cf](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/commit/02619cf))
* unzip not found ([012339e](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/commit/012339e))


### Features

* ability to set additional flags for Ghidra ([9b9aadc](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/commit/9b9aadc))
* Ghidra chart ([b58d8a7](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/commit/b58d8a7))
* Ghidra runs in the bounds set by a cgroup ([dd15724](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/commit/dd15724))
* helm chart ([1c649f1](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/commit/1c649f1))
* Updated for v9.1 ([2221eed](https://gitlab.com/dreamer-labs/iac/dl-ghidra-server/commit/2221eed))
