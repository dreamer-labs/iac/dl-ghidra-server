#!/usr/bin/env bash
UUID=$(cat /proc/sys/kernel/random/uuid)
app_name=ghidra-server-${UUID}
service_name=ghidra-service-${UUID}
claim_name=ghidra-pvc-${UUID}
volume_name=ghidra-block-${UUID}
user_list="ghidra"

usage() {
  echo "usage: $(basename "$0") -u user1 [-u user2]..."
  exit 0
}

if [ $# -eq 0 ]
then
  usage
fi

OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:

while getopts "h?u:" opt; do
  case "$opt" in
  h|\?)
      usage
      exit 0
      ;;
  u)  user_list="${user_list},${OPTARG}"
      ;;
  esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

cat frontend/service.template.yaml | sed -e "s/{{app_name}}/$app_name/g" -e "s/{{service_name}}/$service_name/g" | kubectl create -f -
echo "waiting for service/${service_name}"


_count=0
while [[ $(kubectl get svc ${service_name} | tail -1 | grep -ci pending) -gt 0 ]]
do
  sleep 2
  _count=$((_count+1))
  if [[ _count -ge 50 ]]
  then
    echo "service stuck in pending state!"
    exit 1
  fi
done

# Get the external IP of our load balancer. Ghidra needs this to function.
HOST_IP=$(kubectl get svc ${service_name} --output jsonpath='{.status.loadBalancer.ingress[0].ip}')

if [[ $HOST_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  echo "${service_name} created with EXTERNAL-IP ${HOST_IP}"
else
  echo "unable to retrieve EXTERNAL-IP for ${service_name}"
  exit 1
fi

echo "creating persistent storage volume..."
cat storage/storage.template.yaml  | sed -e "s/{{claim_name}}/$claim_name/g" -e "s/{{volume_name}}/$volume_name/g" | kubectl create -f -

sleep 2
_count=0
while [[ $(kubectl get pvc ${service_name} | tail -1 | grep -ci pending) -gt 0 ]]
do
  sleep 2
  _count=$((_count+1))
  if [[ _count -ge 50 ]]
  then
    echo "service stuck in pending state!"
    exit 1
  fi
done

echo "creating deployment for ${app_name}"
cat backend/deployment.template.yaml | sed -e "s/{{app_name}}/$app_name/g" -e "s/{{HOST_IP}}/$HOST_IP/g" -e "s/{{volume_name}}/$volume_name/g" -e "s/{{claim_name}}/$claim_name/g" -e "s/{{user_list}}/$user_list/g" | kubectl create -f -

kubectl get deployments.apps ${app_name} --output jsonpath='{.status.readyReplicas}'

_count=0
while [[ $(kubectl get deployments.apps ${app_name} --output jsonpath='{.status.readyReplicas}') -eq 0 ]]
do
  sleep 2
  _count=$((_count+1))
  if [[ _count -ge 50 ]]
  then
    echo "deployment not ready!"
    exit 1
  fi
done

echo "${app_name} is listening on ${HOST_IP}:13100"
